import requests
from bs4 import BeautifulSoup
import re
from crawler import write_json, HEADERS

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time

TIME_DELAY = 4

def searchByTopic(topic):
    # e.g. https://www.amazon.de/s?k=liebe&ref=nb_sb_noss
    # Seite 2 https://www.amazon.de/-/en/s?k=liebe&page=2&qid=1619705427&ref=sr_pg_2
    # Seite 3 https://www.amazon.de/-/en/s?k=liebe&page=3&qid=1619707094&ref=sr_pg_3

    # https://www.amazon.de/s?k=liebe&rh=n%3A3510351&dc&language=en&qid=1619705427&rnid=1703609031&ref=sr_nr_n_4
    # Search with category rh=n%3A3510351 with node=3510351
    # node=12950651 => Toys & Games
    # node=11961464031 => Fashion
    # node=3167641 => Home & Kitchen
    
    URL = "https://www.amazon.de/s?k="
    CATEGORIES = ["", "&rh=n%3A12950651", "&rh=n%3A11961464031", "&rh=n%3A3167641"] 
    PAGES = ["", "&page=2", "&page=3"]

    options = Options()
    options.add_argument('--headless')

    browser = webdriver.Firefox(options=options)

    # JSON Data
    data = {}
    data['topic'] = topic
    data['results'] = []

    # Request
    for c in CATEGORIES:
        for p in PAGES:
            #page = requests.get(URL+topic+c+p, headers=HEADERS)
            browser.get(URL+topic+c+p)
            time.sleep(TIME_DELAY+5)
            html = browser.page_source

            #if page.status_code != 200:
            #    print("FAILED To search " + topic + " in amazon page: " + p + " category: " + c)
            #else:
            soup = BeautifulSoup(html, features="lxml")#page.content, features="lxml")

            cresult = {}
            
            cresult['page'] = p
            cresult['realCategory'] = c

            # Check Category
            cresult['category'] = ""
            if c != "":
                # select aria-describedby="searchDropdownDescription"
                # option in select selected="selected" => text shows if right category
                try:
                    select = soup.find_all("select", attrs={"aria-describedby": "searchDropdownDescription"})[0]
                    cresult['cat'] = select.find_all("option", attrs={"selected": "selected"})[0].get_text()
                except:
                    pass

            # div class = a-section a-spacing-small a-spacing-top-small => First One
            # first span contains number of pages and number results 
            cresult['nbrPagesResults'] = ""
            try:
                meta_results = soup.find_all("div", "a-section a-spacing-small a-spacing-top-small")[0] 
                cresult['nbrPagesResults'] = meta_results.find_all("span")[0].get_text()
            except:
                pass

            cresult['results'] = []
            try:
                # div data-component-type="s-search-result" => Each result
                for res in soup.find_all("div", attrs={"data-component-type": "s-search-result"}):
                    result = {}

                    # div has attr data-asin
                    result['asin'] = ""
                    try:
                        result['asin'] = res.get('data-asin')
                    except:
                        #print("E1")
                        pass

                    # TODO Bild

                    # a class = a-link-normal a-text-normal => href link
                    # span in there title
                    result['title'] = ""
                    result['href'] = ""
                    try:
                        tmp = res.find_all("a", "a-link-normal a-text-normal")[0]
                        result['href'] = tmp.get('href')
                        result['title'] = tmp.find_all("span")[0].get('title')
                    except:
                        #print("E2")
                        pass

                    # div class=a-row a-size-base a-color-secondary => text info
                    result['info'] = ""
                    try:
                        result['info'] = res.find_all("div", "a-row a-size-base a-color-secondary")[0].get_text()
                    except:
                        #print("E3")
                        pass

                    # span class=s-label-popover-hover => text Sponsored or not
                    result['sponsored'] = ""
                    try:
                        result['sponsored'] = res.find_all("span", "s-label-popover-hover")[0].get_text()
                    except:
                        #print("E4")
                        pass

                    # span class=a-badge-label-inner a-text-ellipsis => txt Amazons Choice 
                    result['amazonchoice'] = ""
                    try:
                        result['amazonchoice'] = res.find_all("span", "a-badge-label-inner a-text-ellipsis")[0].get_text()
                    except:
                        #print("E5")
                        pass

                    # span with attr aria-label => 1st rating, 2nd nbr ratings (both in attr aria-label)
                    result['rating'] = ""
                    result['nbrRating'] = ""
                    try:
                        result['rating'] = res.find_all("span", attrs={"aria-label": True})[0].get("aria-label")
                        result['nbrRating'] = res.find_all("span", attrs={"aria-label": True})[1].get("aria-label")
                    except:
                        #print("E6")
                        pass

                    # span class="a-price-symbol"
                    # span class="a-price-whole"
                    result['price'] = ""
                    result['priceSymbol'] = ""
                    try:
                        result['price'] = res.find_all("span", "a-price-whole")[0].get_text()
                        result['priceSymbol'] = res.find_all("span", "a-price-symbol")[0].get_text()
                    except:
                        #print("E7")
                        pass
                    
                    result['dataPage'] = getDataByArtikelPage(result['asin'], browser)

                    cresult['results'].append(result)

            except Exception as e:
                print("FAILED To load results " + topic + " in amazon")
                print(str(e))
            
            data['results'].append(cresult)

    # Write JSON DATA
    write_json("./AmzData/"+topic+'_search_amz.json', data)


def getDataByArtikelPage(asin, browser):
    # https://www.amazon.de/dp/B08P1WVDW4 <= asin

    URL = "https://www.amazon.de/dp/"

    # JSON Data
    data = {}
    data['title'] = ""
    data['brand'] = ""
    data['price'] = ""
    data['prime'] = ""
    data['attr'] = []
    data['information'] = []
    data['description'] = ""
    data['ratings'] = []

    #
    browser.get(URL+asin)
    time.sleep(TIME_DELAY+5)
    html = browser.page_source

    # Request
    #page = requests.get(URL+asin, headers=HEADERS)

    #if page.status_code != 200:
    #    print("FAILED To find " + asin + " in amazon")
    #else:
    soup = BeautifulSoup(html, features="lxml")#page.content, features="lxml")


    try:
        # h1 id=title
        data['title'] = soup.find_all("h1", attrs={"id": "title"})[0].get_text()
    except:
        #print("D1")
        pass

    try:
        # a id=bylineInfo
        data['brand'] = soup.find_all("a", attrs={"id": "bylineInfo"})[0].get_text()
    except:
        #print("D2")
        pass

    try:
        # span id=priceblock_ourprice
        data['price'] = soup.find_all("span", attrs={"id": "priceblock_ourprice"})[0].get_text()
    except:
        #print("D3")
        pass

    try:
        # img id=prime-bbop-logo => alt attr
        data['prime'] = soup.find_all("img", attrs={"id": "prime-bbop-logo"})[0].get('alt')
    except:
        #print("D4")
        pass

    try:
        # div id=feature-bullets => li
        for li in soup.find_all("div", attrs={"id": "feature-bullets"})[0].find_all("li"):
            data['attr'] += [li.get_text()]
    except:
        #print("D5")
        pass

    try:
        # table id=productDetails_detailBullets_sections1 => tr
        for tr in soup.find_all("table", attrs={"id": "productDetails_detailBullets_sections1"})[0].find_all("tr"):
            data['information'] += [tr.get_text()]
    except:
        #print("D6")
        pass
    
    try:
        # div id=productDescription => alt attr
        data['description'] = soup.find_all("div", attrs={"id": "productDescription"})[0].get_text()
    except:
        #print("D7")
        pass


    # div id = cm-cr-dp-review-list => Enthält comments
    # div data-hook="review" => Ein comment
    try:
        ratings = soup.find_all("div", attrs={"id": "cm-cr-dp-review-list"})[0].find_all("div", attrs={"data-hook": "review"})
        for rating in ratings:
            review = {}
            
            review['author'] = ""
            try:
                # span class = a-profile-name => text Author
                review['author'] = rating.find_all("span", "a-profile-name")[0].get_text()
            except:
                #print("R1")
                pass

            review['rating'] = ""
            try:
                # a class="a-link-normal" => attr title rating
                review['rating'] = rating.find_all("a", "a-link-normal")[0].get('title')
            except:
                #print("R2")
                pass

            review['title'] = ""
            try:
                # a data-hook="review-title" => text title
                review['title'] = rating.find_all("a", attrs={"data-hook": "review-title"})[0].get_text()
            except:
                #print("R3")
                pass

            review['verified'] = ""
            try:
                # span data-hook="avp-badge-linkless" => txt verified buy
                review['verified'] = rating.find_all("span", attrs={"data-hook": "avp-badge-linkless"})[0].get_text()
            except:
                #print("R4")
                pass

            review['review'] = ""
            try:
                # span data-hook="review-body" => txt review
                    review['review'] = rating.find_all("span", attrs={"data-hook": "review-body"})[0].get_text()
            except:
                #print("R5")
                pass

            review['helpful'] = ""
            try:
                # span data-hook="helpful-vote-statement" => txt amount helpful
                review['helpful'] = rating.find_all("span", attrs={"data-hook", "helpful-vote-statement"})[0].get_text()
            except:
                #print("R6")
                pass

            data['ratings'].append(review)
    except:
        pass

    return data


if __name__ == "__main__":
    #searchByTopic("liebe")

    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    for tag in hashtag_list[1:]:
        print("Crawl " + tag)
        searchByTopic(tag)
    