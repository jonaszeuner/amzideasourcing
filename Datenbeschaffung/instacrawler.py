import requests
from bs4 import BeautifulSoup
import re
from crawler import write_json, HEADERS

def getInstagramData(hashtag):
    #getInstagramDataBySearch(hashtag)
    getInstagramDataByTopHashtags(hashtag)
    getInstagramDataByBestHashtags(hashtag)

def getInstagramDataBySearch(hashtag):
    # TODO NEEDS LOGIN > DEFAULT ACCOUNT AND SELENIUM
    URL = "https://www.instagram.com/explore/tags/"

    # JSON Data
    data = {}
    data['hashtag'] = hashtag
    data['posts'] = []
    data['related'] = []

    # Request
    page = requests.get(URL+hashtag+"/", headers=HEADERS)

    if page.status_code != 200:
        print("FAILED To load hashtag " + hashtag + " in instagram.com")
    else:
        soup = BeautifulSoup(page.content, features="lxml")

        posts = soup.find_all(text=re.compile(" Posts"))
        print(posts)
        pass

        # Write JSON DATA
        write_json("./InstagramData/"+hashtag+'_instagram.json', data)

def getInstagramDataByTopHashtags(hashtag):
    URL = "https://top-hashtags.com/hashtag/"

    # JSON Data
    data = {}
    data['hashtag'] = hashtag
    data['related'] = []
    data['popular'] = []

    # Request
    page = requests.get(URL+hashtag+"/", headers=HEADERS)

    if page.status_code != 200:
        print("FAILED To load hashtag " + hashtag + " in top-hashtags.com")
    else:
        soup = BeautifulSoup(page.content, features="lxml")

        # Related Hashtags
        # id = tht_contain-2
        # Darin ul mit li jeweils related hashtag
        related_hashtags = soup.find(id='tht_contain-2')
        try:
            for r_hashtag in related_hashtags.find_all('li'):
                try:
                    data['related'] += [r_hashtag.get_text()]
                except:
                    print("Fail top-hashtags.com related")
                #print(r_hashtag.get_text())
        except:
            print("Fail top-hashtags.com related")

        # Popular Hashtags used with
        # div mit id = clip-tags-[1-9*]
        # und dann jeweils text
        for i in range(1, 16):
            p_hashtags = soup.find(id='clip-tags-'+str(i))
            try:
                data['popular'].append({str(i): p_hashtags.get_text()})
            except:
                print("Fail top-hashtags.com popular")   
            #print(p_hashtags.get_text())

        # Write JSON DATA
        write_json("./InstagramData/"+hashtag+'_top_hashtags.json', data)

def getInstagramDataByBestHashtags(hashtag):
    URL = "https://best-hashtags.com/hashtag/"

    # JSON Data
    data = {}
    data['hashtag'] = hashtag
    data['popular'] = []
    data['liked'] = []
    data['topTen'] = []
    data['related'] = []

    # Request
    page = requests.get(URL+hashtag+"/", headers=HEADERS)

    if page.status_code != 200:
        print("FAILED To load hashtag " + hashtag + " in best-hashtags.com")
    else:
        soup = BeautifulSoup(page.content, features="lxml")

        # Get most popular and most liked box
        # div with class = tag-box
        boxes = soup.find_all("div", "tag-box")

        try:
            data['popular'] = boxes[0].get_text()
            data['liked'] = boxes[1].get_text()
        except:
            print("Fail best-hashtags.com popular/liked") 

        # Top 10
        # h3 with class = heading-xs
        topTen = soup.find_all("h3", "heading-xs")
        for i in range(len(topTen)):
            try:
                data['topTen'].append({str(i): topTen[i].get_text()})
            except:
                print("Fail best-hashtags.com topTen")    

        # Related Hashtags
        # tbody mit tr als rows und 3 td columns
        try:
            rows = soup.find("tbody").find_all("tr")
            for row in rows:
                cols = row.find_all("td")
                try:
                    data['related'].append({'row': cols[0].get_text(), 'hashtag': cols[1].get_text(), 'posts': cols[2].get_text()})
                except:
                    print("Fail best-hashtags.com related")
        except:
            print("Fail best-hashtags.com related")

        # Write JSON DATA
        write_json("./InstagramData/"+hashtag+'_best_hashtags.json', data)


if __name__ == "__main__":
    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'liebe', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    for tag in hashtag_list:
        getInstagramData(tag)
