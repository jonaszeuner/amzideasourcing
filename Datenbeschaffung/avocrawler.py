import requests
from bs4 import BeautifulSoup
import re
from crawler import write_json, HEADERS
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time

TIME_DELAY = 5

def searchByTopic(topic):
    # e.g. https://www.avocadostore.de/#q=liebe
    # Seite 2 https://www.avocadostore.de/#q=liebe&p=2
    # Seite 3 https://www.avocadostore.de/#q=liebe&p=3

    URL = "https://www.avocadostore.de/#q="
    PAGES = ["", "&p=2", "&p=3"]

    options = Options()
    options.add_argument('--headless')

    browser = webdriver.Firefox(options=options)

    # JSON Data
    data = {}
    data['topic'] = topic
    data['results'] = []

    # Request
    for p in PAGES:
        browser.get(URL+topic+p)
        time.sleep(TIME_DELAY+5)
        html = browser.page_source

        soup = BeautifulSoup(html, features="lxml")

        try:
            
            # div class = exo-result => Enthält ein Ergebnis
            for res in soup.find_all("div", "exo-result"):
                result = {}

                result['page'] = p

                result['id'] = ""
                result['href'] = ""
                try:
                    # Erster a => aid & href mit Link
                    result['id'] = res.find_all("a")[0].get('aid')
                    result['href'] = res.find_all("a")[0].get('href')
                except:
                    print("E1")
                    pass

                # TODO BILD

                result['title'] = ""
                try:
                    # a class = exo-prod-url => Text enthält title
                    result['title'] = res.find_all("a", "exo-prod-url")[0].get_text()
                except:
                    print("E2")
                    pass

                result['brand'] = ""
                try:
                    # div class = exo-product-brand => Brand Name
                    result['brand'] = res.find_all("div", "exo-product-brand")[0].get_text()
                except:
                    print("E3")
                    pass
                
                result['price'] = ""
                try:
                    # div class = exo-product-price => Price
                    result['price'] = res.find_all("div", "exo-product-price")[0].get_text()
                except:
                    print("E4")
                    pass
                
                result['dataPage'] = getDataByArtikelPage(result['href'], browser)

                data['results'].append(result)

        except Exception as e:
            print("FAILED To load results " + topic + " in avo")
            print(str(e))

    # Write JSON DATA
    write_json("./AvoData/"+topic+'_search_avo.json', data)


def getDataByArtikelPage(href, browser):
    # JSON Data
    data = {}
    data['categories'] = []
    data['description'] = ""
    data['criterias'] = []
    data['ratings'] = []
    data['questions'] = []

    # Request
    browser.get(href)
    time.sleep(TIME_DELAY)
    html = browser.page_source

    soup = BeautifulSoup(html, features="lxml")

    # ul class = breadcrumb => li that contain each breadcum stage
    try:
        for li in soup.find_all("ul", "breadcrumb")[0].find_all("li"):
            data['categories'] += [li.get_text()]
    except:
        pass

    # div class = col-product-description => description as text/html
    try:
        data['description'] = soup.find_all("div", "col-product-description")[0].get_text()
    except:
        pass

    # ul class = criteria-list => li contain criteria
    try:
        for li in soup.find_all("ul", "criteria-list")[0].find_all("li"):
            data['criterias'] += [li.get_text()]
    except:
        pass
    
    # div class = product-feedback => Ratings
    try: 
        for comment in soup.find_all("div", "product-feedback"):
            try:
                author = comment.find_all("span", "user-name")[0].get_text()

                date = comment.find_all("div", "product-feedback-meta")[0].get_text()

                com = comment.find_all("div", "product-feedback-text")[0].get_text()

                rating = comment.find_all("meta", attrs={"itemprop": "ratingValue"})[0].get("content")

            except:
                pass
            data['ratings'].append({"author": author, "date": date, "comment": com, "rating": rating})
    except:
        pass

    # div class = product-question => each one question
    try: 
        for question in soup.find_all("div", "product-question"):
            try:
                author = comment.find_all("span", "user-name")[0].get_text()

                date = comment.find_all("div", "product-feedback-meta")[0].get_text()

                com = comment.find_all("div", "product-feedback-text")[0].get_text()

                replys = []
                for rep in comment.find_all("div", "product-feedback-reply"):
                    replys.append({"author": rep.find_all("span", "user-name")[0].get_text(), 
                    "date": rep.find_all("div", "product-feedback-meta")[0].get_text(), 
                    "rep": rep.find_all("div", "product-feedback-text")[0].get_text()})

            except:
                pass
            data['ratings'].append({"author": author, "date": date, "comment": com, "replys": replys})
    except:
        pass
    
    return data


if __name__ == "__main__":
    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'liebe', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    for tag in hashtag_list:
        searchByTopic(tag)
    