import json
from datetime import datetime

HEADERS = ({'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
            'Accept-Language': 'en-US, en;q=0.5'})

def write_json(filename, data):
    data['time'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print(data['time'])
    with open(filename, 'w') as outfile:
        json.dump(data, outfile)