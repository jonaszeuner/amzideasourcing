import requests
from bs4 import BeautifulSoup
import re
from crawler import write_json, HEADERS

def searchByTopic(topic):
    # e.g. https://www.alibaba.com/trade/search?IndexArea=product_en&SearchText=love&f0=y&pricef=&pricet=50
    # Seite 2  https://www.alibaba.com/products/love.html?IndexArea=product_en&page=2&pricet=50
    # Seite 3  https://www.alibaba.com/products/love.html?IndexArea=product_en&page=3&pricet=50

    URL = "https://www.alibaba.com/trade/search?IndexArea=product_en&SearchText="
    URL_ENDING = "&f0=y&pricef=&pricet=50"
    PAGES = ["", "&page=2", "&page=3"]

    # JSON Data
    data = {}
    data['topic'] = topic
    data['results'] = []

    # Request
    for p in PAGES:
        page = requests.get(URL+topic+URL_ENDING+p, headers=HEADERS)

        if page.status_code != 200:
            print("FAILED To search " + topic + " in alibaba page: " + p)
        else:
            soup = BeautifulSoup(page.content, features="lxml")

            # Ergebnisse div class = organic-list app-organic-search__list
            try:
                results = soup.find_all("div", "organic-list app-organic-search__list")[0]
                
                # Mit jew Ereignis div class="list-no-v2-outter J-offer-wrapper"
                for res in results.find_all("div", "list-no-v2-outter J-offer-wrapper"):
                    result = {}

                    result['page'] = p

                    result['id'] = ""
                    result['href'] = ""
                    result['title'] = ""

                    try:
                        # Title h4 class = elements-title-normal__outter => Attr title
                        h4 = res.find_all("h4", "elements-title-normal__outter")[0]
                        result['title'] = h4.get('title')
                        # Link mit attr Href im a innerhalb h4 Element
                        result['href'] = h4.find_all("a")[0].get('href')
                        # Attr data-domdot im a innerhalb h4 Element => id
                        result['id'] = h4.find_all("a")[0].get('data-domdot')

                    except:
                        print("E1")
                    

                    result['price'] = ""
                    result['priceUnit'] = ""
                    try:
                        # Price span class = elements-offer-price-normal__price
                        result['price'] = res.find_all("span", "elements-offer-price-normal__price")[0].get_text()
                        # span class = elements-offer-price-normal__unit
                        result['priceUnit'] = res.find_all("span", "elements-offer-price-normal__unit")[0].get_text()

                    except:
                        print("E2")
                    

                    result['minOrd'] = ""
                    result['minOrdSuffix'] = ""
                    try:
                        # Minord span class = element-offer-minorder-normal__value => text
                        result['minOrd'] = res.find_all("span", "element-offer-minorder-normal__value")[0].get_text()
                        # span class = element-offer-minorder-normal__suffix
                        result['minOrdSuffix'] = res.find_all("span", "element-offer-minorder-normal__suffix")[0].get_text()

                    except:
                        print("E3")
                    
                    result['priceShipping'] = ""
                    result['priceShippingUnit'] = ""
                    try:
                        # Shipping price span class = element-shipping-price__price => text
                        result['priceShipping'] = res.find_all("span", "element-shipping-price__price")[0].get_text()
                        # span class = element-shipping-price__unit
                        result['priceShippingUnit'] = res.find_all("span", "element-shipping-price__unit")[0].get_text()

                    except:
                        print("E4")
                    
                    
                    result['sellerLand'] = ""
                    result['sellerName'] = ""
                    result['sellerLink'] = ""
                    result['sellerYRS'] = ""
                    try:
                        # Land from span class = seller-tag__country flex-no-shrink bg-visible => attr title
                        result['sellerLand'] = res.find_all("span", "seller-tag__country flex-no-shrink bg-visible")[0].get('title')
                        # a flasher-type="supplierName" => text name producer, href link producer
                        result['sellerName'] = res.find_all("a", attrs={"flasher-type": "supplierName"})[0].get_text()
                        result['sellerLink'] = res.find_all("a", attrs={"flasher-type": "supplierName"})[0].get('href')
                        # span class = seller-tag__year flex-no-shrink => text YRS
                        result['sellerYRS'] = res.find_all("span", "seller-tag__year flex-no-shrink")[0].get_text()

                    except:
                        print("E5")

                    data['results'].append(result)

            except Exception as e:
                print("FAILED To load results " + topic + " in alibaba")
                print(str(e))

    # Write JSON DATA
    write_json("./AliData/"+topic+'_search_ali.json', data)


if __name__ == "__main__":
    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'liebe', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    for tag in hashtag_list:
        searchByTopic(tag)
    