from crawler import write_json, HEADERS
import json
from pytrends.request import TrendReq

def getGoogleTrendsData(hashtag, pytrends):
    hashtag = [hashtag]

    pytrends.build_payload(hashtag, cat=0, timeframe='today 5-y', geo='DE')
    data = pytrends.interest_over_time()
    data.index = data.index.strftime("%Y/%m/%d")
    data = data.to_dict()
    write_json("./GoogleTrendsData/"+hashtag[0]+"_gtrends.json", data)

def getGoogleTrendsDataCompared(hashtag, pytrends):
    hashtag = [hashtag, "Google"]

    pytrends.build_payload(hashtag, cat=0, timeframe='today 5-y', geo='DE')
    data = pytrends.interest_over_time()
    data.index = data.index.strftime("%Y/%m/%d")
    data = data.to_dict()
    write_json("./GoogleTrendsData/"+hashtag[0]+"_gtrends_compared.json", data)

if __name__ == "__main__":
    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'liebe', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    pytrends = TrendReq(hl='en-US', tz=360)

    for tag in hashtag_list:
        getGoogleTrendsData(tag, pytrends)
        getGoogleTrendsDataCompared(tag, pytrends)