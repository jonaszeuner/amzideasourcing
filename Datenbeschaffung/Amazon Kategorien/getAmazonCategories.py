import requests
from bs4 import BeautifulSoup
import csv
import time
from random import randrange

HEADERS = ({'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
            'Accept-Language': 'en-US, en;q=0.5'})

CATEGORY_LINKS = "categoryLinks.csv"

CRAWLED_IDS = ['n/11979751031']

URL_BEGINNING = "https://www.amazon.de/"

def writeEntry(cat_id, link, name, depth):
    with open('categories.csv', 'a') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';')
        spamwriter.writerow([cat_id, link, name, depth])

def getAttrOfListElement(li):
    # GET ID
    cat_id = li.get('id')

    # GET Link
    link = li.find_all('a')
    if len(link) > 0:
        cat_link = link[0].get('href')
    else:
        cat_link = "ENDELEMENT"
            
    # Get Name
    cat_name = ""
    for span in li.find_all('span'):
        cat_name += span.get_text()
    cat_name = cat_name.replace("\n", " ")
    
    #print(cat_id, cat_link, cat_name)

    return cat_id, cat_link, cat_name

def getListOfCategories(soup):
    category_div = soup.find(id='departments')
    #print(CRAWLED_IDS)
    return category_div.find_all('li')

def crawl(link, depth=0):
    global CRAWLED_IDS

    if URL_BEGINNING in link:
        page = requests.get(link, headers=HEADERS)
    else:
        page = requests.get(URL_BEGINNING+link, headers=HEADERS)

    if page.status_code != 200:
        print("FAILED TO LOAD AMAZON CATEGORY " + cat)
    else:
        soup = BeautifulSoup(page.content, features="lxml")

        # GET Kategorie List
        cats = getListOfCategories(soup)

        for li in cats:
            cat_id, cat_link, cat_name = getAttrOfListElement(li)

            if cat_id not in CRAWLED_IDS:
                writeEntry(cat_id, cat_link, cat_name, depth)
                CRAWLED_IDS += [cat_id]
                if cat_link != "ENDELEMENT" and depth < 1:
                    time.sleep(randrange(5))
                    crawl(cat_link, (depth+1))
                
def main():
    categories = ["https://www.amazon.de/-/en/s?i=popular&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_0",
    "https://www.amazon.de/-/en/s?i=stripbooks&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_1", 
    "https://www.amazon.de/-/en/s?i=dvd&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_2", 
    "https://www.amazon.de/-/en/s?i=english-books&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_3",
    "https://www.amazon.de/-/en/s?i=classical&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_4",
    "https://www.amazon.de/-/en/s?i=videogames&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_5",
    "https://www.amazon.de/-/en/s?i=shoes&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_6",
    "https://www.amazon.de/-/en/s?i=clothing&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_7",
    "https://www.amazon.de/-/en/s?i=luggage&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_8",
    "https://www.amazon.de/-/en/s?i=jewelry&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_9",
    "https://www.amazon.de/-/en/s?i=watches&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_10",
    "https://www.amazon.de/-/en/s?i=electronics&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_11",
    "https://www.amazon.de/-/en/s?i=kitchen&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_12",
    "https://www.amazon.de/-/en/s?i=diy&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_13",
    "https://www.amazon.de/-/en/s?i=drugstore&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_14",
    "https://www.amazon.de/-/en/s?i=industrial&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_15",
    "https://www.amazon.de/-/en/s?i=software&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_16",
    "https://www.amazon.de/-/en/s?i=toys&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_17",
    "https://www.amazon.de/-/en/s?i=computers&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_18",
    "https://www.amazon.de/-/en/s?i=sports&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_19",
    "https://www.amazon.de/-/en/s?i=office-products&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_20",
    "https://www.amazon.de/-/en/s?i=beauty&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_21",
    "https://www.amazon.de/-/en/s?i=outdoor&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_22",
    "https://www.amazon.de/-/en/s?i=grocery&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_23",
    "https://www.amazon.de/-/en/s?i=automotive&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_24",
    "https://www.amazon.de/-/en/s?i=baby&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_25",
    "https://www.amazon.de/-/en/s?i=pets&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_26",
    "https://www.amazon.de/-/en/s?i=lighting&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_27",
    "https://www.amazon.de/-/en/s?i=appliances&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_28",
    "https://www.amazon.de/-/en/s?i=digital-text&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_29",
    "https://www.amazon.de/-/en/s?i=mi&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_30",
    "https://www.amazon.de/-/en/s?i=gift-cards&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_31",
    "https://www.amazon.de/-/en/s?i=misc&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_32",
    "https://www.amazon.de/-/en/s?i=handmade&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_33",
    "https://www.amazon.de/-/en/s?i=magazines&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_34",
    "https://www.amazon.de/-/en/s?i=digital-music&bbn=11979751031&dc&qid=1619262046&ref=lp_specialty-aps_nr_i_35"]
    
    for cat in categories:
        print(cat)
        crawl(cat)

if __name__ == "__main__":
    main()