import requests
from bs4 import BeautifulSoup
import re
from crawler import write_json, HEADERS

def searchByTopic(topic):
    # e.g. https://www.etsy.com/search?q=liebe&explicit=1&max=50&ship_to=DE
    #  Seite 2 https://www.etsy.com/search?q=liebe&explicit=1&max=50&ship_to=DE
    #  Seite 3 https://www.etsy.com/search?q=liebe&explicit=1&max=50&ship_to=DE&page=3&ref=pagination

    URL = "https://www.etsy.com/search?q="
    URL_ENDING = "&explicit=1&max=50&ship_to=DE"
    PAGES = ["", "&page=2&ref=pagination", "&page=3&ref=pagination"]

    # JSON Data
    data = {}
    data['topic'] = topic
    data['results'] = []

    # Request
    for p in PAGES:
        page = requests.get(URL+topic+URL_ENDING+p, headers=HEADERS)

        if page.status_code != 200:
            print("FAILED To search " + topic + " in etsy page: " + p)
        else:
            soup = BeautifulSoup(page.content, features="lxml")

            # ul class = wt-grid wt-grid--block wt-pl-xs-0 tab-reorder-container => beinhaltet Ergebnisse
            try:
                results = soup.find_all("ul", "wt-grid wt-grid--block wt-pl-xs-0 tab-reorder-container")[0]
                #print(results)
                
                # div class = js-merch-stash-check-listing v2-listing-card => enthält ein Ergebnis
                for res in results.find_all("div", "js-merch-stash-check-listing v2-listing-card wt-mr-xs-0 search-listing-card--desktop"):
                    result = {}

                    result['page'] = p

                    result['id'] = ""
                    result['href'] = ""
                    try:
                        # a class = listing-link => Attribut href mit Link zum Artikel, Attr data-listing-id mit ID von Artikel
                        result['id'] = res.find_all("a", "listing-link")[0].get('data-listing-id')
                        result['href'] = res.find_all("a", "listing-link")[0].get('href')
                    except:
                        print("E1")
                        pass

                    # <img data-listing-card-listing-image => ist Bild
                    # TODO

                    result['title'] = ""
                    try:
                        # h3 class = wt-mb-xs-0 wt-text-truncate wt-text-caption => Enthält Titel
                        result['title'] = res.find_all("h3", "wt-mb-xs-0 wt-text-truncate wt-text-caption")[0].get_text()
                    except:
                        print("E2")
                        pass

                    result['rating'] = ""
                    try:
                        # input name = initial-rating => Attribut value Rating
                        result['rating'] = res.find_all("input", attrs={"name": "initial-rating"})[0].get("value")
                    except:
                        print("E3")
                        pass

                    result['amountRating'] = ""
                    try:
                        # span class= wt-text-body-01 wt-text-gray => Enthält Anzahl Bewertungen
                        result['amountRating'] = res.find_all("span", "wt-text-body-01 wt-text-gray")[0].get_text()
                    except:
                        print("E4")
                        pass

                    result['price'] = ""
                    try:
                        # span class = currency-value => Enthält Preis
                        result['price'] = res.find_all("span", "currency-value")[0].get_text()
                    except:
                        print("E5")
                        pass
                    
                    result['dataPage'] = getDataByArtikelPage(result['id'])

                    data['results'].append(result)

            except Exception as e:
                print("FAILED To load results " + topic + " in etsy")
                print(str(e))

    # Write JSON DATA
    write_json("./EtsyData/"+topic+'_search_etsy.json', data)


def getDataByArtikelPage(pID):
    # https://www.etsy.com/de/listing/932098408  <= Artikel ID aus Übersicht

    URL = "https://www.etsy.com/de/listing/"

    # JSON Data
    data = {}
    data['details'] = []
    data['description'] = ""
    data['amountSales'] = ""
    data['comments'] = []

    # Request
    page = requests.get(URL+pID, headers=HEADERS)

    if page.status_code != 200:
        print("FAILED To find " + pID + " in etsy")
    else:
        soup = BeautifulSoup(page.content, features="lxml")

        # div id = product-details-content-toggle enthält ul mit li mit Produkt Informationen e.g. Handgefertigt
        try:
            for li in soup.find_all("div", attrs={"id": "product-details-content-toggle"})[0].find_all("li"):
                data['details'] += [li.get_text()]
        except:
            pass

        # <p data-product-details-description-text-content => enthält Produktbeschreibung
        # div data-id = description-text
        try:
            data['description'] = soup.find_all("div", attrs={"data-id": "description-text"})[0].get_text()
        except:
            pass

        # div id = listing-page-cart; darin span class = wt-text-caption => enthält Anzahl Verkäufe
        try:
            data['amountSales'] = soup.find_all("div", attrs={"id", "listing-page-cart"})[0].find_all("span", "wt-text-caption")[0].get_text()
        except:
            pass

        # div class = wt-grid wt-grid--block wt-mb-xs-0 => Enthält comments
        # div class = wt-grid__item-xs-12 wt-mb-xs-4 => Ein comment
        try:
            comments = soup.find_all("div", "wt-grid wt-grid--block wt-mb-xs-0")[0].find_all("div", "wt-grid__item-xs-12 wt-mb-xs-4")
            for comment in comments:
                try:
                    # p class = wt-text-caption wt-text-gray => Author and Date 
                    authDate = comment.find_all("p", "wt-text-caption wt-text-gray")[0].get_text()

                    # p class = wt-text-truncate--multi-line wt-break-word => Comment
                    com = comment.find_all("p", "wt-text-truncate--multi-line wt-break-word")[0].get_text()

                    # <input type="hidden" name="initial-rating" value="5"> => Rating
                    rating = comment.find_all("input", attrs={"name": "initial-rating"})[0].get("value")

                except:
                    pass
                data['comments'].append({"authorDate": authDate, "comment": com, "rating": rating})
        except:
            pass
    
    return data


if __name__ == "__main__":
    hashtag_list = ['london', 'fussball', 'soccer', 'fantasy', 'weihnachten', 'christmas', 'liebe', 'love', 'vegan', 'hund', 'dog', 
    'katze', 'cat', 'auto', 'car', 'kaffee', 'coffee']

    for tag in hashtag_list:
        searchByTopic(tag)
    